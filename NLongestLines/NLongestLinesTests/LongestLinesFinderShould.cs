﻿using Domain;
using NUnit.Framework;

namespace NLongestLinesTests
{
    [TestFixture]
    public class LongestLinesFinderShould
    {
        private static LongestLinesFinder CreateFinder()
        {
            return new LongestLinesFinder();
        }

        [Test]
        public void Return_2_LongestLines_IfInput_IsLines()
        {
            var finder = CreateFinder();

            var result = finder.GetLongestLines(new[] {"22", "333", "1", "4444"}, 2);

            Assert.AreEqual(new[] {"4444", "333"}, result);
        }

        [Test]
        public void Return_N_LongestLines_IfInput_IsLines()
        {
            var finder = CreateFinder();

            var result = finder.GetLongestLines(new[] {"22", "333", "1", "4444"}, 3);

            Assert.AreEqual(new[] {"4444", "333", "22"}, result);
        }

        [Test]
        public void Return_N_LongestLines_UsingLinq_IfInput_IsLines()
        {
            var finder = CreateFinder();

            var result = finder.GetLongestLinesLinq(new[] {"22", "333", "1", "4444"}, 3);

            Assert.AreEqual(new[] {"4444", "333", "22"}, result);
        }

        [Test]
        public void ReturnArrayOfLinesLengthes_IfInputIsArray()
        {
            var finder = CreateFinder();

            var result = finder.GetLineLength(new[] {"aa", "aaa", "aa"});

            Assert.AreEqual(new[] {2, 3, 2}, result);
        }

        [Test]
        public void ReturnLengthOfLine_IsInput_IsLine()
        {
            var finder = CreateFinder();

            var result = finder.GetLineLength("12345");

            Assert.AreEqual(5, result);
        }

        [Test]
        public void ReturnLongestLine_IfInput_IsLines()
        {
            var finder = CreateFinder();

            var result = finder.GetLongestLine(new[] {"22", "333", "1", "4444"});

            Assert.AreEqual("4444", result);
        }

        [Test]
        public void ReturnSortedLines_IfInputIsLines()
        {
            var finder = CreateFinder();

            var result = finder.SortLines(new[] {"22", "333", "1"});

            Assert.AreEqual(new[] {"333", "22", "1"}, result);
        }

        [Test]
        public void ReturnZero_IfInput_IsEmpty()
        {
            var finder = CreateFinder();

            var result = finder.GetLineLength(string.Empty);

            Assert.AreEqual(0, result);
        }
    }
}