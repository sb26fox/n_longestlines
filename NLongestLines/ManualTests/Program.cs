﻿using System;
using System.Collections.Generic;
using System.IO;
using Domain;

namespace ManualTests
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            var finder = new LongestLinesFinder();

            var tmp = File.ReadAllLines(args[0]);
            var lines = new List<string>();

            for (var i = 1; i < tmp.Length; i++)
                lines.Add(tmp[i]);

            var result = finder.GetLongestLinesLinq(lines.ToArray(), int.Parse(tmp[0]));

            foreach (var s in result)
            {
                Console.WriteLine(s);
            }
        }
    }
}