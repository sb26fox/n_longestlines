﻿using System.Collections.Generic;
using System.Linq;

namespace Domain
{
    public class LongestLinesFinder
    {
        public double GetLineLength(string input)
        {
            return input.Length;
        }

        public int[] GetLineLength(string[] input)
        {
            var lengthes = new List<int>();

            foreach (var str in input)
            {
                lengthes.Add(str.Length);
            }

            return lengthes.ToArray();
        }

        public string[] SortLines(string[] input)
        {
            var lengthes = GetLineLength(input);


            for (var i = 0; i < lengthes.Length; i++)
            {
                for (var j = i; j < lengthes.Length; j++)
                {
                    if (lengthes[j] > lengthes[i])
                    {
                        var tmp = input[i];
                        input[i] = input[j];
                        input[j] = tmp;
                    }
                }
            }

            return input;
        }

        public string GetLongestLine(string[] input)
        {
            var lines = SortLines(input);

            return input[0];
        }


        public string[] GetLongestLines(string[] input, int n)
        {
            var lines = SortLines(input);
            var longestLines = new List<string>();

            for (var i = 0; i < n; i++)
            {
                longestLines.Add(lines[i]);
            }

            return longestLines.ToArray();
        }

        public string[] GetLongestLinesLinq(string[] input, int n)
        {
            var sorted = from i in input
                        orderby i.Length descending
                        select i;

            var lines = sorted.ToArray();

            var longestLines = new List<string>();

            for (var i = 0; i < n; i++)
            {
                longestLines.Add(lines[i]);
            }

            return longestLines.ToArray();
        }
    }
}